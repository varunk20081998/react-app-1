import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  FlatList,
} from "react-native";
import Header from "../components/Header";
import Footer from "../components/Footer";

export default function Home({ navigation, GlobalState }) {
  const { toDoList, SetToDoList, task, SetTask, SetChosenTask } = GlobalState;

  const renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        style={styles.task}
        onPress={() => HandleChooseTask(item)}
      >
        <Text>{item.task}</Text>
      </TouchableOpacity>
    );
  };

  const HandleSaveTask = () => {
    const index = toDoList.length + 1;
    SetToDoList((prev) => [...prev, { id: index, task: task }]);
    SetTask("");
  };
  const HandleChooseTask = (item) => {
    SetChosenTask(item);
    navigation.navigate("ChosenTask");
  };
  return (
    <View style={styles.screen}>
      <Header />
      <View style={styles.body}>
        <TextInput
          style={styles.input}
          onChangeText={SetTask}
          value={task}
          placeholder="Enter your task"
        />
        <TouchableOpacity
          style={styles.button}
          onPress={() => HandleSaveTask()}
        >
          <Text style={styles.buttonText}>Submit</Text>
        </TouchableOpacity>
        <FlatList
          data={toDoList}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
        />
      </View>
      <Footer navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },

  body: {
    flex: 8,
    width: "100%",
    backgroundColor: "#14141405",
  },
  task: {
    backgroundColor: "white",
    padding: 10,
    margin: 10,
    borderRadius: 12,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    zIndex: 5,
  },
  input: {
    backgroundColor: "white",
    padding: 15,
    paddingBottom: 10,
    paddingTop: 10,
    marginTop: 30,
    margin: 10,
    borderRadius: 12,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    zIndex: 5,
  },
  button: {
    alignItems: "center",
    backgroundColor: "#141414",
    padding: 15,
    paddingBottom: 10,
    paddingTop: 10,
    marginBottom: 30,
    margin: 10,
    borderRadius: 12,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.23,
    shadowRadius: 2.62,
    elevation: 4,
    zIndex: 5,
  },
  buttonText: {
    color: "white",
    fontWeight: "900",
  },
});
