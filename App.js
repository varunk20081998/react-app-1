import React, { useState, useEffect } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import registerNNPushToken from "native-notify";
import Home from "./src/screens/Home";
import ChosenTask from "./src/screens/ChosenTask";

const Stack = createNativeStackNavigator();

export default function App() {
  // push notifications
  registerNNPushToken(4675, "iH0tF3XoZFTLedtNxhcuDd");
  //globalstate management

  const [toDoList, SetToDoList] = useState([{ id: 1, task: "one thing" }]);
  const [task, SetTask] = useState("");
  const [chosenTask, SetChosenTask] = useState("");

  const globalstate = {
    toDoList,
    SetToDoList,
    task,
    SetTask,
    chosenTask,
    SetChosenTask,
  };

  //navigation
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" options={{ headerShown: false }}>
          {(props) => <Home {...props} GlobalState={globalstate} />}
        </Stack.Screen>

        <Stack.Screen name="ChosenTask" options={{ headerShown: false }}>
          {(props) => <ChosenTask {...props} GlobalState={globalstate} />}
        </Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
